FROM mcr.microsoft.com/dotnet/sdk:6.0   as build 

RUN mkdir /build
COPY mqttRelay.cs  /build/mqttRelay.cs  
COPY mqttRelay.csproj /build/
COPY mqttRelay.sln /build/
COPY Program.cs /build/

RUN cd /build && dotnet restore mqttRelay.sln
RUN cd /build && dotnet build  mqttRelay.sln --configuration Release
RUN cd /build && dotnet publish mqttRelay.sln --configuration Release --output /dist --no-restore

FROM mcr.microsoft.com/dotnet/runtime:6.0 as final

COPY ./ecc-private.key /app/ecc-private.key
COPY ./ecc-server.crt /app/ecc-server.crt
COPY  --from=build /dist /app
EXPOSE 8885/tcp
CMD ["/app/mqttRelay"]

