﻿

using MQTTRelay.Server;


internal class Program
{
    public static async Task Main(string[] args)
    {
        try
        {
            await MqttServer.RunRelay();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}