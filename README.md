# MQTT Relay / Bridge 

 broker to bridge local connections on port 8885 to an azure IOT hub or azure iot edge or other brokers. The bridge broker uses TLS with a TLS ECC server certificate. To connect an AD009 you have to upload the AD009_ca.pem to the device. 

start the container (e.g. using docker compose). Make sure you set these environment variables
* MQTT_CLIENT_ID = Device name in azure iot hub
* MQTT_BROKER_URI = your mqtt host name the bridge will publish the messages to
* MQTT_USER = user name to authenticate to azure iot-edge or iot hub
  * for iot hub it looks like this: AZURE_HUB_NAME.azure-devices.net/AZURE_DEVICE_NAME/?api-version=2021-04-12
* MQTT_PASSWORD = sas token for your azure iot device
  * should look like this: SharedAccessSignature sr=AZURE_HUB_NAME.azure-devices.net...
* AD009_USER = username of AD009 to connect to this bridge
* AD009_PASSWORD = password of AD009 to connect to this bridge
  * if you do specify neither AD009_USER nor AD009_PASSWORD, then client credential validation is disabled
* VALIDATE_BROKER_TLS set to false, to disable TLS valition to the azure broker