using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Logging;
using MQTTnet.Internal;
using MQTTnet.Server;
using MQTTnet.Client;
using MQTTnet.Packets;
using MQTTnet.Protocol;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Diagnostics;
using MQTTnet;
namespace MQTTRelay.Server;

public static class MqttServer
{
    class MqttLogger : IMqttNetLogger
    {
        private readonly ILogger<MqttLogger> _logger;

        public MqttLogger(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<MqttLogger>();
        }

        public void Publish(MqttNetLogLevel logLevel, string source, string message, object[] parameters, Exception exception)
        {
            var mappedLevel = LogLevel.Information;

            switch (logLevel)
            {
                case MqttNetLogLevel.Error:
                    mappedLevel = LogLevel.Error;
                    break;
                case MqttNetLogLevel.Info:
                    mappedLevel = LogLevel.Information;
                    break;
                case MqttNetLogLevel.Verbose:
                    mappedLevel = LogLevel.Debug;
                    break;
                case MqttNetLogLevel.Warning:
                    mappedLevel = LogLevel.Warning;
                    break;
            }

            _logger.Log(mappedLevel, exception, message, parameters);
        }

        public bool IsEnabled { get; } = true;
    }



    public static async Task RunRelay()
    {

        using ILoggerFactory factory = LoggerFactory.Create(builder => builder.AddConsole());
        var mqttFactory = new MqttFactory(new MqttLogger(factory));

        ILogger clientLogger = factory.CreateLogger("client");
        using (var mqttClient = mqttFactory.CreateManagedMqttClient())
        {

            string devName = Environment.GetEnvironmentVariable("MQTT_CLIENT_ID");
            string brokerUri = Environment.GetEnvironmentVariable("MQTT_BROKER_URI");
            string user = Environment.GetEnvironmentVariable("MQTT_USER");
            string pw = Environment.GetEnvironmentVariable("MQTT_PASSWORD");
            string AD009_USER = Environment.GetEnvironmentVariable("AD009_USER");
            string AD009_PASSWORD = Environment.GetEnvironmentVariable("AD009_PASSWORD");
            string VALIDATE_BROKER_TLS = Environment.GetEnvironmentVariable("VALIDATE_BROKER_TLS");
            string LISTEN_PORT = Environment.GetEnvironmentVariable("LISTEN_PORT");


            // clientLogger.LogInformation(devName);
            // clientLogger.LogInformation(brokerUri);
            // clientLogger.LogInformation(user);
            // //clientLogger.LogInformation(pw);


            var brokerUriParts = brokerUri.Split(":");
            string brokerHost = brokerUriParts[0];
            int port = 8883;
            if (brokerUriParts.Length > 1)
            {
                port = int.Parse(brokerUriParts[1]);
            }


            var mqttClientOptionsBuilder = new MqttClientOptionsBuilder()
                .WithTcpServer(brokerHost, port).WithClientId(devName).WithCredentials(user, pw);
            if (VALIDATE_BROKER_TLS != null)
            {
                if (String.Equals(VALIDATE_BROKER_TLS, "false", StringComparison.OrdinalIgnoreCase))
                {
                    mqttClientOptionsBuilder.WithTlsOptions(new MqttClientTlsOptionsBuilder().UseTls().WithAllowUntrustedCertificates()
                        .WithCertificateValidationHandler(
                            args => { return true; }).Build());
                }
            }

            var mqttClientOptions = mqttClientOptionsBuilder.Build();

            var managedMqttClientOptions = new ManagedMqttClientOptionsBuilder()
                .WithClientOptions(mqttClientOptions)
                .Build();
            mqttClient.ConnectedAsync += async args =>
            {
                clientLogger.LogInformation(args.ToString());
            };

            await mqttClient.StartAsync(managedMqttClientOptions);

            var certPEM = X509Certificate2.CreateFromPemFile(
                "/app/ecc-server.crt",
                "/app/ecc-private.key");

            var cert = new X509Certificate2(certPEM.Export(X509ContentType.Pfx));
            //clientLogger.LogInformation(cert.ToString());

            int listPort = 8885;

            if (LISTEN_PORT != null)
            {
                listPort = int.Parse(LISTEN_PORT);
            }

            var mqttServerOptions = new MqttServerOptionsBuilder().WithEncryptedEndpoint()
                .WithEncryptedEndpointPort(listPort)
                .WithEncryptionCertificate(cert).Build();



            using (var mqttServer = mqttFactory.CreateMqttServer(mqttServerOptions))
            {
                mqttServer.ValidatingConnectionAsync += args =>
                {
                    if (AD009_USER == null && AD009_PASSWORD == null)
                    {
                        args.ReasonCode = MqttConnectReasonCode.Success;
                        return Task.CompletedTask;
                    }
                    if (String.Equals(args.UserName, AD009_USER) && String.Equals(args.Password, AD009_PASSWORD))
                    {
                        args.ReasonCode = MqttConnectReasonCode.Success;
                        return Task.CompletedTask;
                    }
                    args.ReasonCode = MqttConnectReasonCode.BadUserNameOrPassword;
                    return Task.CompletedTask;
                };
                mqttServer.InterceptingPublishAsync += async args =>
                {
                    string payload = args.ApplicationMessage.ConvertPayloadToString();
                    await mqttClient.EnqueueAsync(args.ApplicationMessage.Topic, payload);
                };

                clientLogger.LogInformation("Starting ");
                await mqttServer.StartAsync();

                clientLogger.LogInformation("Started");
                do
                {
                    System.Threading.Thread.Sleep(10000);
                } while (true);
                //clientLogger.LogInformation("Started");
                //await mqttServer.StopAsync();
            }
        }
    }
}